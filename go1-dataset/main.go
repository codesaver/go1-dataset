package main

import (
	"phuonghuynh-eng/go1-dataset/handler"

	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	lambda.Start(handler.S3EventHandle)
}
